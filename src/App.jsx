import { useEffect } from "react"
import { useDispatch, useSelector } from "react-redux"
import "./assets/styles/index.scss"
import "./assets/styles/media.scss"
import FullScreenLoader from "./components/FullScreenLoader"
import MainBlock from "./components/MainBlock"
import Sidebar from "./components/Sidebar"
import { updateWeatherInfo } from "./redux/actions/weatherActions"
import "./redux/watchers/currentCityWatcher"

function App() {
  const dispatch = useDispatch()
  const weatherType = useSelector(state => state.weather.weatherInfo?.weather ? state.weather.weatherInfo.weather[0].main.toLowerCase() : null)

  useEffect(() => {
    dispatch(updateWeatherInfo())
  }, [])

  

  return (
    <div className="App">
      <div className={`wrapper ${weatherType}`}>
        <FullScreenLoader />
        <MainBlock />
        <Sidebar />
      </div>
    </div>
  )
}

export default App
