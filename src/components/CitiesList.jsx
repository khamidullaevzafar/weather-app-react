import { useDispatch, useSelector } from "react-redux"
import { setCurrentCity } from "../redux/reducers/citiesReducer"
import Loader from "./Loader"

const CitiesList = () => {
  const dispatch = useDispatch()
  const citiesList = useSelector((state) => state.cities.citiesList)
  const listLoader = useSelector((state) => state.app.listLoader)

  const cityClickHandler = (city) => {
    dispatch(setCurrentCity(city))
  }

  return (
    <div className="cities-block">
      {listLoader ? (
        <div className="loader-block ">
          <Loader visible/>
        </div>
      ) : (
        <div className="cities-list">
          {citiesList.length ? (
            citiesList.map((city) => (
              <div
                className="city transparent-text"
                key={city.id}
                onClick={() => cityClickHandler(city)}
              >
                {city.name} <span className="country">{city.sys?.country}</span> 
              </div>
            ))
          ) : (
            <div className="transparent-text">
              Nothing was found for your search
            </div>
          )}
        </div>
      )}
    </div>
  )
}

export default CitiesList
