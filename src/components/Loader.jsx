

const Loader = ({visible}) => {

  if(!visible) return null

  return (
    <div className="loader">
      <div className="lds-facebook"><div></div><div></div><div></div></div>
    </div>
  )
}

export default Loader
