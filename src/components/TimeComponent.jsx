import { useEffect, useState } from "react"

const TimeComponent = () => {
  const [currentTime, setCurrentTime] = useState(new Date())

  useEffect(() => {
    const interval = setInterval(() => {
      setCurrentTime(new Date())
    }, 1000)

    return () => {
      clearInterval(interval)
    }
  }, [])

  return <div className="time">{currentTime.toLocaleString()}</div>
}

export default TimeComponent
