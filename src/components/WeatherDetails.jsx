import { useSelector } from "react-redux"

const WeatherDetails = () => {
  const weatherInfo = useSelector(state => state.weather.weatherInfo)

  return (
    <div className="weather-details">
      <div className="title white-text">Weather Details</div>
      <div className="row">
        <div className="detail-name transparent-text">Cloudy</div>
        <div className="detail-value white-text">{weatherInfo?.clouds?.all} %</div>
      </div>
      <div className="row">
        <div className="detail-name transparent-text">Humidity</div>
        <div className="detail-value white-text">{weatherInfo?.main?.humidity} %</div>
      </div>
      <div className="row">
        <div className="detail-name transparent-text">Wind</div>
        <div className="detail-value white-text">{weatherInfo?.wind?.speed} km/h</div>
      </div>
      <div className="row">
        <div className="detail-name transparent-text">Pressure</div>
        <div className="detail-value white-text">{weatherInfo?.main?.pressure} hPa</div>
      </div>
    </div>
  )
}

export default WeatherDetails
