import CitiesList from "./CitiesList"
import SearchForm from "./SearchForm"
import WeatherDetails from "./WeatherDetails"


const Sidebar = () => {
  return (
    <div className="sidebar-wrapper">
        <div className="sidebar">
          <SearchForm />

          <CitiesList />

          <WeatherDetails />
        </div>
      </div>
  )
}

export default Sidebar
