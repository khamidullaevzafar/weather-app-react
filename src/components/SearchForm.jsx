import { useEffect } from "react"
import { useState } from "react"
import { useDispatch } from "react-redux"
import { searchCities } from "../redux/actions/citiesAction"
import { resetCitiesList } from "../redux/reducers/citiesReducer"
import searchIcon from '../assets/images/search-solid.svg'

const SearchForm = () => {
  const dispatch = useDispatch()
  const [searchText, setSearchText] = useState('')

  useEffect(() => {
    if(searchText==='') dispatch(resetCitiesList())
  }, [searchText])


  const submitHandler = (e) => {
    e.preventDefault()
    dispatch(searchCities(searchText))
  }

  return (
    <form className="search-block" onSubmit={submitHandler}>
      <input
        type="text"
        className="search-input"
        placeholder="Another location"
        value={searchText}
        onChange={e => setSearchText(e.target.value)}
      />
      <button type="submit" className="submit-btn"><img src={searchIcon} alt="" /></button>
    </form>
  )
}

export default SearchForm
