import { useSelector } from "react-redux"
import Loader from "./Loader"


const FullScreenLoader = () => {
  const visible = useSelector(state => state.app.fullScreenLoader)

  if(!visible) return null

  return (
    <div className="fullscreen-loader">
      <Loader visible/>
    </div>
  )
}

export default FullScreenLoader
