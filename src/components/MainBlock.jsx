import { data } from "browserslist"
import { useSelector } from "react-redux"
import cloudIcon from '../assets/images/cloud-icon.svg'
import smogIcon from '../assets/images/smog-icon.svg'
import snowIcon from '../assets/images/snow-icon.svg'
import thunderstormIcon from '../assets/images/thunderstorm-icon.svg'
import rainIcon from '../assets/images/rain-icon.svg'
import clearIcon from '../assets/images/clear-icon.svg'
import TimeComponent from "./TimeComponent"

const icons = {
  clouds: cloudIcon,
  clear: clearIcon,
  atmosphere: smogIcon,
  snow: snowIcon,
  rain: rainIcon,
  drizzle: rainIcon,
  thunderstorm: thunderstormIcon
}

const MainBlock = () => {
  const weatherInfo = useSelector(state => state.weather.weatherInfo)
  const weatherType = useSelector(state => state.weather.weatherInfo?.weather ? state.weather.weatherInfo.weather[0].main.toLowerCase() : null)

  return (
    <div className="main-block">
      <div className="info-block">
        <div className="temp">{Math.round(weatherInfo?.main?.temp)}&#176;</div>
        <div className="city-info">
          <div className="city-name">{weatherInfo?.name}</div>
          <TimeComponent />
        </div>
        <div className="weather-info">
          <div className="weather-icon">
            <img src={icons[weatherType]} alt="" />
          </div>
          <div className="weather-name">{weatherInfo?.weather ? weatherInfo.weather[0].main : ""}</div>
        </div>
      </div>
    </div>
  )
}

export default MainBlock
