import axios from "axios"
import { API_KEY, BASE_URL } from "../../config"
import { hideListLoader, showListLoader } from "../reducers/appReducer"
import { setCitiesList } from "../reducers/citiesReducer"



export const searchCities = (searchText) => {
  return async dispatch => {
    dispatch(showListLoader())
    try {
      const response = await axios({
        method: "GET",
        baseURL: BASE_URL,
        url: "/find",
        params: {
          appid: API_KEY,
          q: searchText
        }
      })
      dispatch(setCitiesList(response.data.list))
    } catch (error) {
      console.log(error)
    }
    finally {
      dispatch(hideListLoader())
    }
  }
}