import axios from "axios"
import { API_KEY, BASE_URL, UNITS } from "../../config"
import { hideFullScreenLoader, showFullScreenLoader } from "../reducers/appReducer"
import { setWeatherInfo } from "../reducers/weatherReducer"
import { store } from '../store'

export const updateWeatherInfo = () => {
  return async dispatch => {
    const cityID = store.getState().cities.currentCity.id

    dispatch(showFullScreenLoader())
    try {
      const response = await axios({
        method: "GET",
        baseURL: BASE_URL,
        url: "/weather",
        params: {
          appid: API_KEY,
          units: UNITS,
          id: cityID
        }
      })
      dispatch(setWeatherInfo(response.data))
    } catch (error) {
      console.log(error)
    }
    finally {
      dispatch(hideFullScreenLoader())
    }
  }
}