import watch from 'redux-watch'
import { updateWeatherInfo } from '../actions/weatherActions'
import { store } from '../store'

let w = watch(store.getState, 'cities.currentCity')
store.subscribe(w((newVal, oldVal, objectPath) => {
  localStorage.setItem('currentCity', JSON.stringify(newVal))
  store.dispatch(updateWeatherInfo())
}))