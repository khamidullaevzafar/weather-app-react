import { combineReducers } from "redux"
import appReducer from "./appReducer"
import citiesReducer from "./citiesReducer"
import weatherReducer from "./weatherReducer"

export const rootReducer = combineReducers({
  app: appReducer,
  weather: weatherReducer,
  cities: citiesReducer
})
