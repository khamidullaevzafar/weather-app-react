import { SET_WEATHER_INFO } from "../actionTypes"


const initialState = {
  weatherInfo: {}
}

export default function weatherReducer(state = initialState, action) {
  switch (action.type) {
    case SET_WEATHER_INFO:
      return {
        ...state,
        weatherInfo: action.payload
      }

    default:
      return state
  }
}

export const setWeatherInfo = info => ({type: SET_WEATHER_INFO, payload: info})
