import citiesOfUzbekistan from '../../utils/citiesOfUzbekistan.json'
import { RESET_CITIES_LIST, SET_CITIES_LIST, SET_CURRENT_CITY } from '../actionTypes'

const initialState = {
  citiesList: citiesOfUzbekistan,
  currentCity: JSON.parse(localStorage.getItem('currentCity')) ?? citiesOfUzbekistan[0]
}

export default function citiesReducer(state = initialState, action) {
  switch (action.type) {
    case SET_CITIES_LIST:
      return {
        ...state,
        citiesList: action.payload
      }

    case RESET_CITIES_LIST:
      return {
        ...state,
        citiesList: citiesOfUzbekistan
      }
  
    case SET_CURRENT_CITY:
      return {
        ...state,
        currentCity: action.payload
      }

    default:
      return state
  }
}

export const setCitiesList = list => ({type: SET_CITIES_LIST, payload: list})
export const resetCitiesList = () => ({type: RESET_CITIES_LIST})
export const setCurrentCity = city => ({type: SET_CURRENT_CITY, payload: city})