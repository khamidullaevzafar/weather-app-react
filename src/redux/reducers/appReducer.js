import { HIDE_FULL_SCREEN_LOADER, HIDE_LIST_LOADER, SHOW_FULL_SCREEN_LOADER, SHOW_LIST_LOADER } from "../actionTypes"


const initialState = {
  fullScreenLoader: false,
  listLoader: false
}

export default function appReducer(state = initialState, action) {
  switch (action.type) {
    case SHOW_FULL_SCREEN_LOADER:
      return {
        ...state,
        fullScreenLoader: true
      }
    
    case HIDE_FULL_SCREEN_LOADER:
      return {
        ...state,
        fullScreenLoader: false
      }

    case SHOW_LIST_LOADER: 
      return {
        ...state,
        listLoader: true
      }

    case HIDE_LIST_LOADER:
      return {
        ...state,
        listLoader: false
      }
  
    default:
      return state
  }
}

export const showFullScreenLoader = () => ({type: SHOW_FULL_SCREEN_LOADER})
export const hideFullScreenLoader = () => ({type: HIDE_FULL_SCREEN_LOADER})
export const showListLoader = () => ({type: SHOW_LIST_LOADER})
export const hideListLoader = () => ({type: HIDE_LIST_LOADER})